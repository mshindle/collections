package dbutil

import (
	"context"
	"database/sql"
)

type Scanner interface {
	Scan(dest ...interface{}) error
}

type ScannerFunc func(dest ...interface{}) error

func (f ScannerFunc) Scan(dest ...interface{}) error {
	return f(dest)
}

type Database interface {
	Query(query string, args ...interface{}) (*sql.Rows, error)
	QueryRow(query string, args ...interface{}) *sql.Row
	Exec(query string, args ...interface{}) (sql.Result, error)
	ExecContext(ctx context.Context, query string, args ...interface{}) (sql.Result, error)
}

type Rows interface {
	Close() error
	Err() error
	Next() bool
	Scan(dest ...interface{}) error
}

func ScanRows(r Rows, f func(row Scanner) error) error {
	var closeErr error
	defer func() {
		if err := r.Close(); err != nil {
			closeErr = err
		}
	}()

	var scanErr error
	for r.Next() {
		err := f(r)
		if err != nil {
			scanErr = err
			break
		}
	}
	if r.Err() != nil {
		return r.Err()
	}
	if scanErr != nil {
		return scanErr
	}

	return closeErr
}
