package stream

import (
	"context"
	"fmt"
	"reflect"
	"testing"
)

var (
	numbers = []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	evens   = []int{0, 2, 4, 6, 8}
)

func TestSimple(t *testing.T) {
	ctx := context.Background()
	want := []int{0, 1, 2, 3, 4}
	result := ToSlice(ctx,
		Transform(ctx, func(n int) int { return n / 2 },
			Filter(ctx, isEven,
				FromSlice(ctx, numbers))))

	if !reflect.DeepEqual(result, want) {
		t.Errorf("incorrect result. got: %v, want: %v", result, want)
		return
	}
	fmt.Printf("%v", result)
}

func TestFilter(t *testing.T) {
	ctx := context.Background()
	result := ToSlice(ctx,
		Filter(ctx, isEven,
			FromSlice(ctx, numbers)))

	if !reflect.DeepEqual(result, evens) {
		t.Errorf("incorrect result. got: %v, want: %v", result, evens)
		return
	}
	fmt.Printf("%v", result)
}

// helper functions for the tests...

func isEven(n int) bool {
	return n%2 == 0
}
