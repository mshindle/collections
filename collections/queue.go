package collections

import (
	"errors"
	"sync"
)

// queueNode is single linked list node
type queueNode[T any] struct {
	data T
	next *queueNode[T]
}

// Queue is a collection designed for holding elements prior to processing.
type Queue[T any] struct {
	head  *queueNode[T]
	tail  *queueNode[T]
	count int
	lock  sync.Mutex
}

var (
	// ErrQueueEmpty is returned to indicate the queue is empty which isn't necessarily an "error" state.
	ErrQueueEmpty = errors.New("queue is empty")
)

// NewQueue creates a new Queue
func NewQueue[T any]() *Queue[T] {
	return new(Queue[T])
}

// Len returns the size of the queue
func (q *Queue[T]) Len() int {
	q.lock.Lock()
	defer q.lock.Unlock()
	return q.count
}

// Peek returns the head of the queue without removing it from the queue
func (q *Queue[T]) Peek() (T, error) {
	q.lock.Lock()
	defer q.lock.Unlock()
	if q.head == nil {
		var noop T
		return noop, ErrQueueEmpty
	}
	return q.head.data, nil
}

// Push adds an item to the end of the queue
func (q *Queue[T]) Push(item T) {
	q.lock.Lock()
	defer q.lock.Unlock()

	n := &queueNode[T]{data: item}

	if q.tail == nil {
		q.head = n
	} else {
		q.tail.next = n
	}
	q.tail = n
	q.count++
}

// Poll removes an item from the head of the queue and returns it
func (q *Queue[T]) Poll() (T, error) {
	q.lock.Lock()
	defer q.lock.Unlock()

	if q.head == nil {
		var noop T
		return noop, ErrQueueEmpty
	}

	n := q.head
	q.head = n.next
	q.count--

	if q.head == nil {
		q.tail = nil
	}

	return n.data, nil
}
