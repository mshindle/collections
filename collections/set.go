package collections

// Set defines a collection of comparable items.
type Set[T comparable] map[T]struct{}

// NewSet creates a Set of type T.
func NewSet[T comparable]() Set[T] {
	return make(Set[T])
}

// NewSetWithSize creates a Set of type T and the specified size.
func NewSetWithSize[T comparable](size int) Set[T] {
	return make(Set[T], size)
}

// NewSetWithItems creates a set populated with items.
func NewSetWithItems[T comparable](items ...T) Set[T] {
	set := NewSetWithSize[T](len(items))
	set.AddItems(items...)
	return set
}

// Add an item to the set
func (a Set[T]) Add(item T) {
	a[item] = struct{}{}
}

// AddItems adds multiple items to the set
func (a Set[T]) AddItems(items ...T) {
	for _, item := range items {
		a[item] = struct{}{}
	}
}

// DeleteItem removes an item from the set. If the item does not exist in the set,
// DeleteItem is effectively a no-op.
func (a Set[T]) DeleteItem(item T) {
	delete(a, item)
}

// HasItem checks if the set contains the item
func (a Set[T]) HasItem(item T) bool {
	_, ok := a[item]
	return ok
}

// Union returns a set combining the two sets. The larger set is modified
// to contain the additional items from the smaller set. To leave the original
// sets unmodified, use UnionNew.
func (a Set[T]) Union(b Set[T]) Set[T] {
	bigger, smaller := orderSets(a, b)
	for item := range smaller {
		bigger.Add(item)
	}
	return bigger
}

// UnionNew returns a set combining the two sets.
func (a Set[T]) UnionNew(b Set[T]) Set[T] {
	set := NewSetWithSize[T](len(a) + len(b))
	for item := range a {
		set.Add(item)
	}
	for item := range b {
		set.Add(item)
	}
	return set
}

// Intersect returns a new set with the elements in common between sets A and B.
func (a Set[T]) Intersect(b Set[T]) Set[T] {
	bigger, smaller := orderSets(a, b)
	set := NewSet[T]()

	for item := range smaller {
		if bigger.HasItem(item) {
			set.Add(item)
		}
	}

	return set
}

// Sub returns a new set with the items in b removed from a.
func (a Set[T]) Sub(b Set[T]) Set[T] {
	set := NewSet[T]()

	for item := range a {
		if !b.HasItem(item) {
			set.Add(item)
		}
	}

	return set
}

// orderSets returns the sets in descending order of size. If the sets are equal size,
// the first listed set is returned as bigger.
func orderSets[T comparable](a, b Set[T]) (Set[T], Set[T]) {
	bigger, smaller := a, b
	if len(b) > len(a) {
		bigger, smaller = b, a
	}
	return bigger, smaller
}
