# Collections

Useful data structures and collections which seem to be used in multiple projects.

## LinkedList

## Queue

## Set

A Set is an unordered collection of unique items. Following the implementations used in languages like Java, we use a map (dictionary) as the backing for our set to make lookups a constant operation. Our `map` uses an empty struct to keep memory and performance low. See [this article](https://levelup.gitconnected.com/memory-allocation-and-performance-in-golang-maps-b267b5ad9217) for a nice write-up comparing empty structs to interfaces.

Set implements 3 basic set functions.

 1. Union (A ⋃ B) — is the Set that contains all the elements in Set A and B. Two versions of Union are implemented: one which modifies one of the sets and the other which creates a new set.
 2. Intersection (A ∩ B)— is the Set that contains all the elements that are in Set A and B. Implemented as Intersect.
 3. Difference (A − B) — is the Set of the elements that are in A and not B. Implemented as Sub.

## Tree

## Thanks
After reading [Noah Schumacher's article](https://betterprogramming.pub/utilizing-sets-with-golang-generics-316b4fe2ca28), I decided to get organized and put all the data structures into one repository and Golang library. I also updated the code to take advantage of generics as I found that I had duplicated that implementation across multiple projects.