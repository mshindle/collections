package collections

import (
	"errors"
	"reflect"
	"testing"
)

func TestNewRingBuffer(t *testing.T) {
	type testCase[T any] struct {
		name     string
		capacity int
		want     *RingBuffer[T]
	}
	tests := []testCase[int]{
		{
			name:     "simple",
			capacity: 5,
			want:     &RingBuffer[int]{data: make([]int, 5)},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewRingBuffer[int](tt.capacity); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewRingBuffer() = %v, want %v", got, tt.want)
			}
		})
	}
}

func createBuffer(capacity int, items ...int) *RingBuffer[int] {
	r := RingBuffer[int]{data: make([]int, capacity)}
	for i, item := range items {
		r.data[i] = item
		r.end = i + 1
	}
	return &r
}

func TestRingBuffer_Cap(t *testing.T) {
	type testCase[T any] struct {
		name string
		r    *RingBuffer[T]
		want int
	}
	tests := []testCase[int]{
		{
			name: "simple",
			r:    createBuffer(5, 10, 20),
			want: 5,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.r.Cap(); got != tt.want {
				t.Errorf("Cap() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRingBuffer_Len(t *testing.T) {

	type testCase[T any] struct {
		name string
		r    *RingBuffer[T]
		want int
	}
	tests := []testCase[int]{
		{
			name: "empty",
			r:    createBuffer(5),
			want: 0,
		},
		{
			name: "three_items",
			r:    createBuffer(5, 1, 2, 3),
			want: 3,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.r.Len(); got != tt.want {
				t.Errorf("Len() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRingBuffer_Peek(t *testing.T) {
	type testCase[T any] struct {
		name    string
		r       RingBuffer[T]
		want    T
		wantErr bool
	}
	tests := []testCase[int]{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.r.Peek()
			if (err != nil) != tt.wantErr {
				t.Errorf("Peek() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Peek() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRingBuffer_Poll(t *testing.T) {
	type testCase[T any] struct {
		name    string
		r       RingBuffer[T]
		want    T
		wantErr bool
	}
	tests := []testCase[int]{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.r.Poll()
			if (err != nil) != tt.wantErr {
				t.Errorf("Poll() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Poll() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRingBuffer_Push(t *testing.T) {
	type testCase struct {
		name     string
		capacity int
		items    []string
		wantErr  bool
		expError error
	}
	tests := []testCase{
		{
			name:     "one_item",
			capacity: 5,
			items:    []string{"alpha"},
			wantErr:  false,
		},
		{
			name:     "three_items",
			capacity: 5,
			items:    []string{"alpha", "bravo", "charlie"},
			wantErr:  false,
		},
		{
			name:     "six_items_error",
			capacity: 5,
			items:    []string{"alpha", "bravo", "charlie", "delta", "echo", "foxtrot"},
			wantErr:  true,
			expError: ErrBufferFull,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var r = NewRingBuffer[string](tt.capacity)
			var err error

			for _, item := range tt.items {
				if err = r.Push(item); err != nil {
					break
				}
			}
			if (err != nil) != tt.wantErr {
				t.Errorf("Push() error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.wantErr {
				if !errors.Is(err, tt.expError) {
					t.Errorf("Push() wrong error, got = %v, want: %v", err, tt.expError)
				}
				return
			}
			if got := r.Len(); got != len(tt.items) {
				t.Errorf("Count not incremented properly: got = %v, want = %v", got, len(tt.items))
				return
			}
		})
	}
}

func BenchmarkRingBuffer(b *testing.B) {
	rr := NewRingBuffer[int](100_000)
	for n := 0; n < b.N; n++ {
		if rr.isFull {
			_, _ = rr.Poll()
		}
		_ = rr.Push(n)
	}
}

func BenchmarkArray(b *testing.B) {
	var ar [100_000]int
	size := 0

	for n := 0; n < b.N; n++ {
		if size >= len(ar) {
			copy(ar[0:], ar[1:])
			size--
		}

		ar[size] = n
		size++
	}
}
