package collections

import (
	"fmt"
	"strings"
)

// TraversalType defines which type of traversal should occur on a tree
type TraversalType int

const (
	PreOrder TraversalType = 1 << iota
	InOrder
	PostOrder
)

// Visit will execute on each node traversed
type Visit func(n *Node)

// Comparable imposes a total ordering on all types which implement it. This
// ordering is referred to as the type's natural ordering.
type Comparable interface {
	// CompareTo compares this type with the specified type for order. Returns a -1, 0, or 1 if this type
	// is less than, equal to, or greater than the specified type.
	CompareTo(o Comparable) int
}

// Node is an element on the tree with two children
type Node struct {
	Left  *Node
	Right *Node
	Key   Comparable
}

// NewNode creates a new node object from the given key
func NewNode(key Comparable) *Node {
	return &Node{Key: key}
}

// String creates a string representation of the node and its children
func (n *Node) String() string {
	if n.Key == nil {
		return "[]"
	}

	var sb strings.Builder

	sb.WriteString("[")
	if n.Left != nil {
		sb.WriteString(n.Left.String())
		sb.WriteString(" ")
	}
	sb.WriteString(fmt.Sprintf("%v", n.Key))
	if n.Right != nil {
		sb.WriteString(" ")
		sb.WriteString(n.Right.String())
	}
	sb.WriteString("]")

	return sb.String()
}

// Insert adds a node to the tree.
func (n *Node) Insert(node *Node) *Node {
	if n == nil {
		return node
	}
	if n.Key.CompareTo(node.Key) <= 0 {
		n.Right = n.Right.Insert(node)
	} else {
		n.Left = n.Left.Insert(node)
	}
	return n
}

// TraversePre visits the current node before visiting the left nodes
// and then the right nodes
func (n *Node) TraversePre(visit Visit) {
	if n == nil {
		return
	}
	visit(n)
	n.Left.TraversePre(visit)
	n.Right.TraversePre(visit)
}

// TraverseIn walks the tree in order - visit the furthest left nodes, the current node, and then the right nodes.
func (n *Node) TraverseIn(visit Visit) {
	if n == nil {
		return
	}
	n.Left.TraverseIn(visit)
	visit(n)
	n.Right.TraverseIn(visit)
}

// TraversePost walks all the child nodes before processing itself - left nodes first
func (n *Node) TraversePost(visit Visit) {
	if n == nil {
		return
	}
	n.Left.TraversePost(visit)
	n.Right.TraversePost(visit)
	visit(n)
}

// Tree creates a "rooted" binary tree. While an implementation can just hold the root node directly,
type Tree struct {
	Root *Node
}

// NewTree creates a new tree object from the given root
func NewTree(root *Node) *Tree {
	return &Tree{Root: root}
}

// String creates a string representation of the tree
func (tr *Tree) String() string {
	return tr.Root.String()
}

// Insert inserts a Node to a Tree without replacement.
func (tr *Tree) Insert(nd *Node) {
	if tr.Root == nd {
		return
	}
	tr.Root = tr.Root.Insert(nd)
}

// Traverse moves across the tree executing visit on each node as determined by TraversalType
func (tr *Tree) Traverse(visit Visit, t TraversalType) {
	switch t {
	case PreOrder:
		tr.Root.TraversePre(visit)
	case InOrder:
		tr.Root.TraverseIn(visit)
	case PostOrder:
		tr.Root.TraversePost(visit)
	}

}
