package collections

import (
	"fmt"
	"reflect"
	"strings"
	"testing"
)

type testString string

func (a testString) CompareTo(o Comparable) int {
	b := o.(testString)
	if a < b {
		return -1
	}
	if a == b {
		return 0
	}
	return 1
}

var testStrings = []Comparable{
	testString("F"),
	testString("B"),
	testString("G"),
	testString("A"),
	testString("D"),
	testString("I"),
	testString("H"),
	testString("C"),
	testString("E"),
}

func TestNewNode(t *testing.T) {
	type args struct {
		key Comparable
	}
	tests := []struct {
		name string
		args args
		want *Node
	}{
		{
			name: "create a simple node",
			args: args{key: testStrings[0]},
			want: &Node{Key: testStrings[0]},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewNode(tt.args.key); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewNode() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNode_Insert(t *testing.T) {
	buildNodes := func(c []Comparable) *Node {
		n := &Node{Key: c[0]}
		n.Left = &Node{Key: c[1]}
		return n
	}

	type fields struct {
		Left  *Node
		Right *Node
		Key   Comparable
	}
	type args struct {
		node *Node
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *Node
	}{
		{
			name:   "add_one_node",
			fields: fields{Left: nil, Right: nil, Key: testStrings[0]},
			args:   args{node: &Node{Key: testStrings[1]}},
			want:   buildNodes(testStrings[:2]),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			n := &Node{
				Left:  tt.fields.Left,
				Right: tt.fields.Right,
				Key:   tt.fields.Key,
			}
			if got := n.Insert(tt.args.node); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Insert() = %v, want %v", got, tt.want)
			}
		})
	}
}

func createTree(keys []Comparable) *Tree {
	tree := new(Tree)
	for _, key := range keys {
		tree.Insert(NewNode(key))
	}

	return tree
}

func TestNode_String(t *testing.T) {
	tests := []struct {
		name  string
		input *Tree
		want  string
	}{
		{
			name:  "single_node",
			input: createTree(testStrings[:1]),
			want:  "[F]",
		},
		{
			name:  "two_children",
			input: createTree(testStrings[:3]),
			want:  "[[B] F [G]]",
		},
		{
			name:  "whole_list",
			input: createTree(testStrings),
			want:  "[[[A] B [[C] D [E]]] F [G [[H] I]]]",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.input.Root.String(); got != tt.want {
				t.Errorf("String() = %v, want %v", got, tt.want)
			}
		})
	}
}

type crumbtrail struct {
	sep     string
	builder strings.Builder
}

func newCrumbtrail(sep string) *crumbtrail {
	return &crumbtrail{sep: sep}
}

func (c *crumbtrail) Visit(n *Node) {
	c.builder.WriteString(fmt.Sprintf("%s%v", c.sep, n.Key))
}

func (c *crumbtrail) ToString() string {
	return c.builder.String()[len(c.sep):]
}

func TestNode_TraverseIn(t *testing.T) {
	tests := []struct {
		name     string
		rootNode *Node
		want     string
	}{
		{
			name:     "single_node",
			rootNode: createTree(testStrings[:1]).Root,
			want:     "F",
		},
		{
			name:     "two_children",
			rootNode: createTree(testStrings[:3]).Root,
			want:     "B F G",
		},
		{
			name:     "whole_list",
			rootNode: createTree(testStrings).Root,
			want:     "A B C D E F G H I",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			trail := newCrumbtrail(" ")
			tt.rootNode.TraverseIn(trail.Visit)
			if got := trail.ToString(); got != tt.want {
				t.Errorf("String() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNode_TraversePost(t *testing.T) {
	tests := []struct {
		name     string
		rootNode *Node
		want     string
	}{
		{
			name:     "single_node",
			rootNode: createTree(testStrings[:1]).Root,
			want:     "F",
		},
		{
			name:     "two_children",
			rootNode: createTree(testStrings[:3]).Root,
			want:     "B G F",
		},
		{
			name:     "whole_list",
			rootNode: createTree(testStrings).Root,
			want:     "A C E D B H I G F",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			trail := newCrumbtrail(" ")
			tt.rootNode.TraversePost(trail.Visit)
			if got := trail.ToString(); got != tt.want {
				t.Errorf("String() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNode_TraversePre(t *testing.T) {
	tests := []struct {
		name     string
		rootNode *Node
		want     string
	}{
		{
			name:     "single_node",
			rootNode: createTree(testStrings[:1]).Root,
			want:     "F",
		},
		{
			name:     "two_children",
			rootNode: createTree(testStrings[:3]).Root,
			want:     "F B G",
		},
		{
			name:     "whole_list",
			rootNode: createTree(testStrings).Root,
			want:     "F B A D C E G I H",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			trail := newCrumbtrail(" ")
			tt.rootNode.TraversePre(trail.Visit)
			if got := trail.ToString(); got != tt.want {
				t.Errorf("String() = %v, want %v", got, tt.want)
			}
		})
	}
}
