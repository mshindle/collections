package collections

import (
	"errors"
	"reflect"
	"testing"
)

func TestNewQueue(t *testing.T) {
	type testCase[T any] struct {
		name string
		want *Queue[T]
	}
	tests := []testCase[int]{
		{
			name: "simple",
			want: &Queue[int]{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewQueue[int](); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewQueue() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestQueue_Len(t *testing.T) {
	node := &queueNode[int]{data: 1}
	type testCase struct {
		name string
		q    *Queue[int]
		want int
	}
	tests := []testCase{
		{
			name: "simple",
			want: 1,
			q:    &Queue[int]{head: node, tail: node, count: 1},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.q.Len(); got != tt.want {
				t.Errorf("Len() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestQueue_Push(t *testing.T) {
	type testCase struct {
		name  string
		items []int
	}
	tests := []testCase{
		{
			name:  "one_item",
			items: []int{10},
		},
		{
			name:  "three_items",
			items: []int{10, 20, 30},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			q := NewQueue[int]()
			for _, item := range tt.items {
				q.Push(item)
			}
			if got := q.Len(); got != len(tt.items) {
				t.Errorf("Count not incremented properly: got = %v, want = %v", got, len(tt.items))
				return
			}
			node := q.head
			for _, item := range tt.items {
				if item != node.data {
					t.Errorf("wrong element in queue: got = %v, want = %v", node.data, item)
					return
				}
				node = node.next
			}
			if node != nil {
				t.Errorf("too many elements in queue: node = %v, want nil", node)
			}
		})
	}
}

func createQueue[T any](items ...T) *Queue[T] {
	q := NewQueue[T]()
	for _, item := range items {
		q.Push(item)
	}
	return q
}

func TestQueue_Poll(t *testing.T) {
	items := []string{"alpha", "bravo", "charlie", "delta"}
	type testCase struct {
		name    string
		toPush  int
		wantErr bool
	}
	tests := []testCase{
		{
			name:   "zero_item",
			toPush: 0,
		},
		{
			name:   "one_item",
			toPush: 1,
		},
		{
			name:   "four_item",
			toPush: 4,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			q := createQueue[string](items[:tt.toPush]...)
			// check that we get an ErrQueueEmpty response
			if tt.toPush == 0 {
				_, err := q.Poll()
				if !errors.Is(err, ErrQueueEmpty) {
					t.Errorf("Poll() error = %v, wantErr %v", err, ErrQueueEmpty)
					return
				}
			}

			for i := 0; i < tt.toPush; i++ {
				got, err := q.Poll()
				if (err != nil) != tt.wantErr {
					t.Errorf("Poll() error = %v, wantErr %v", err, tt.wantErr)
					return
				}
				if got != items[i] {
					t.Errorf("Poll() got = %v, want %v", got, items[i])
					return
				}
			}
		})
	}
}

func TestQueue_Peek(t *testing.T) {
	items := []string{"alpha", "bravo", "charlie", "delta"}
	type testCase struct {
		name    string
		toPush  int
		toSkip  int
		want    string
		wantErr bool
	}
	tests := []testCase{
		{
			name:    "zero_item",
			toPush:  0,
			toSkip:  0,
			want:    "",
			wantErr: true,
		},
		{
			name:    "one_item",
			toPush:  1,
			toSkip:  0,
			want:    items[0],
			wantErr: false,
		},
		{
			name:    "four_item",
			toPush:  4,
			toSkip:  2,
			want:    items[2],
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			q := createQueue[string](items[:tt.toPush]...)
			for i := 0; i < tt.toSkip; i++ {
				_, err := q.Poll()
				if err != nil {
					t.Errorf("unexpected error while polling")
					return
				}
			}

			got, err := q.Peek()
			if (err != nil) != tt.wantErr {
				t.Errorf("Peek() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Peek() got = %v, want %v", got, tt.want)
				return
			}

			// confirm that when we poll, we got the same value as when we peeked
			got, err = q.Poll()
			if (err != nil) != tt.wantErr {
				t.Errorf("Poll after peek error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Poll after peek() got = %v, want %v", got, tt.want)
				return
			}
		})
	}
}
