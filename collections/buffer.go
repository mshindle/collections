package collections

import (
	"errors"
	"fmt"
)

var (
	ErrBufferFull  = errors.New("out of bounds; buffer is full")
	ErrBufferEmpty = errors.New("buffer is empty")
)

// RingBuffer is an array based circular data structure that has a pointer to next available slot.
// It is filled with pre-allocated transfer objects. Producers and consumers perform writing and reading
// of data to the ring without locking or contention.
type RingBuffer[T any] struct {
	data   []T  // container data of a generic type T
	isFull bool // disambiguate whether the queue is full or empty
	start  int  // start index (inclusive, i.e. first element)
	end    int  // end index (exclusive, i.e. next after last element)
}

// NewRingBuffer creates a RingBuffer with defined capacity and type.
func NewRingBuffer[T any](capacity int) *RingBuffer[T] {
	return &RingBuffer[T]{
		data:   make([]T, capacity), // buffer allocation
		isFull: false,               // non-full at start
		start:  0,                   // start with 0 index
		end:    0,                   // end with 0 as well
	}
}

func (r *RingBuffer[T]) Push(elem T) error {
	if r.isFull {
		return ErrBufferFull
	}

	r.data[r.end] = elem
	r.end = (r.end + 1) % len(r.data)
	r.isFull = r.end == r.start

	return nil
}

// Peek returns the head of the buffer without removing it. Returns
// ErrBufferEmpty and a zero value if empty.
func (r *RingBuffer[T]) Peek() (T, error) {
	var res T // "zero" element (respective of the type)
	if !r.isFull && r.start == r.end {
		return res, ErrBufferEmpty
	}
	res = r.data[r.start]
	return res, nil
}

// Poll returns the head of the buffer and removes it. Returns
// ErrBufferEmpty and a zero value if empty.
func (r *RingBuffer[T]) Poll() (T, error) {
	var res T // "zero" element (respective of the type)
	if !r.isFull && r.start == r.end {
		return res, ErrBufferEmpty
	}

	res = r.data[r.start]                 // copy over the first element in the queue
	r.start = (r.start + 1) % len(r.data) // move the start of the queue
	r.isFull = false                      // since we're removing elements, we can never be full

	return res, nil
}

// Len returns the size of the buffer
func (r *RingBuffer[T]) Len() int {
	l := r.end - r.start
	if l < 0 || (l == 0 && r.isFull) {
		l = len(r.data) - l
	}

	return l
}

// Cap returns the capacity of the buffer
func (r *RingBuffer[T]) Cap() int {
	return len(r.data)
}

func (r *RingBuffer[T]) IsFull() bool {
	return r.isFull
}

func (r *RingBuffer[T]) String() string {
	return fmt.Sprintf("[RingBuffer full:%v capacity:%d len:%d start:%d end:%d]",
		r.isFull, r.Cap(), r.Len(), r.start, r.end)
}
