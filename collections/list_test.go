package collections

import "testing"

var listElements = []string{"alpha", "bravo", "charlie"}

func TestList_Create(t *testing.T) {
	ll := NewLinkedList[string](listElements...)

	i := 0
	for item := ll.head; item != nil; item = item.Next {
		if item.Value != listElements[i] {
			t.Errorf("got value '%v', expected '%s'", item.Value, listElements[i])
			return
		}
		i++
	}
}

func TestLinkedList_Prepend(t *testing.T) {
	ll := NewLinkedList[string]()

	for _, elem := range listElements {
		ll.Prepend(&Item[string]{Value: elem})
	}

	item := ll.head
	for i := len(listElements) - 1; i >= 0; i-- {
		if item.Value != listElements[i] {
			t.Errorf("got value '%v', expected '%s'", item.Value, listElements[i])
			return
		}
		item = item.Next
	}
}

func TestLinkedList_Append(t *testing.T) {
	ll := NewLinkedList[string]()

	for _, elem := range listElements {
		ll.Append(&Item[string]{Value: elem})
	}

	i := 0
	for item := ll.head; item != nil; item = item.Next {
		if item.Value != listElements[i] {
			t.Errorf("got value '%v', expected '%s'", item.Value, listElements[i])
			return
		}
		i++
	}
}
