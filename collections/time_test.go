package collections

import (
	"testing"
	"time"
)

func elapseCheck(t *testing.T, d time.Duration) func(e time.Duration) {
	return func(e time.Duration) {
		if e.Truncate(time.Second) != d {
			t.Errorf("not expected elapsed time - got: %v, want: %v", e, d)
		}
	}
}

func TestTrackElapsed(t *testing.T) {
	d := 1 * time.Second
	t.Run("sleep", func(t *testing.T) {
		defer TrackElapsed(time.Now(), elapseCheck(t, d))
		time.Sleep(d)
	})
}
