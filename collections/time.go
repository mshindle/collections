package collections

import "time"

// TrackElapsed is a helpful function for executing logic around tracking execution time.
// A simple (and most common) example involves passing a logging that records the elapsed
// time on upon completion.
//
//	 func logElapsed(e time.Duration) {
//			log.Printf("elapsed = %v", e)
//	 }
//
//		func longProcess() {
//		 	defer TrackElapsed(time.Now(), logElapsed)
//			time.Sleep(2 * time.Second)
//	 }
func TrackElapsed(start time.Time, f func(time.Duration)) {
	f(time.Since(start))
}
