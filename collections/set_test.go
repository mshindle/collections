package collections

import "testing"

const benchEntries = 100

func mapWithInterface() {
	m := map[int]interface{}{}
	for i := 1; i <= benchEntries; i++ {
		m[i] = nil
	}
}

func mapWithEmptyStruct() {
	m := map[int]struct{}{}
	for i := 1; i <= benchEntries; i++ {
		m[i] = struct{}{}
	}
}

func Benchmark_Interface(b *testing.B) {
	for i := 0; i < b.N; i++ {
		mapWithInterface()
	}
}

func Benchmark_EmptyStruct(b *testing.B) {
	for i := 0; i < b.N; i++ {
		mapWithEmptyStruct()
	}
}
