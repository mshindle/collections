package collections

type Item[T any] struct {
	Value T
	Next  *Item[T]
	Prev  *Item[T]
}

type LinkedList[T any] struct {
	head *Item[T]
	tail *Item[T]
}

func (l *LinkedList[T]) Prepend(item *Item[T]) *Item[T] {
	head := l.head
	l.head = item
	item.Next = head

	if head != nil {
		head.Prev = item
	}

	if l.tail == nil {
		l.tail = item
	}

	return item
}

func (l *LinkedList[T]) Append(item *Item[T]) *Item[T] {
	tail := l.tail
	l.tail = item

	if tail != nil {
		tail.Next = item
	}

	if l.head == nil {
		l.head = item
	}

	return item
}

func (l *LinkedList[T]) Shift() *Item[T] {
	head := l.head

	if head != nil {
		l.head = head.Next
	}

	if l.head == nil {
		l.tail = nil
	}

	return head
}

func (l *LinkedList[T]) Head() *Item[T] {
	return l.head
}

func (l *LinkedList[T]) Insert(before *Item[T], item *Item[T]) *Item[T] {
	prev := before.Prev
	before.Prev = item
	if prev != nil {
		prev.Next = item
	}

	return item
}

func (l *LinkedList[T]) Remove(item *Item[T]) {
	if item == nil {
		return
	}

	if item == l.head && item == l.tail {
		l.head = nil
		l.tail = nil
	} else if item == l.head {
		l.head = l.head.Next
		l.head.Prev = nil
	} else if item == l.tail {
		l.tail = l.tail.Prev
		l.tail.Next = nil
	} else {
		item.Prev.Next, item.Next.Prev = item.Next, item.Prev
	}
}

func NewLinkedList[T any](values ...T) *LinkedList[T] {
	list := &LinkedList[T]{}

	if len(values) == 0 {
		return list
	}
	prev := &Item[T]{Value: values[0]}
	list.head = prev
	list.tail = prev

	for i := 1; i < len(values); i++ {
		item := &Item[T]{Value: values[i]}
		prev.Next = item
		item.Prev = prev
		prev = item
		list.tail = item
	}
	return list
}
